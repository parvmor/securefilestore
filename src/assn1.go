package assn1

// You MUST NOT change what you import.  If you add ANY additional
// imports it will break the autograder, and we will be Very Upset.

import (

	// You neet to add with
	// go get github.com/fenilfadadu/CS628-assn1/userlib
	"github.com/fenilfadadu/CS628-assn1/userlib"

	// Life is much easier with json:  You are
	// going to want to use this so you can easily
	// turn complex structures into strings etc...
	"encoding/json"

	// Likewise useful for debugging etc
	"encoding/hex"

	// UUIDs are generated right based on the crypto RNG
	// so lets make life easier and use those too...
	//
	// You need to add with "go get github.com/google/uuid"
	"github.com/google/uuid"

	// Useful for debug messages, or string manipulation for datastore keys
	"strings"

	// Want to import errors
	"errors"
)

// This serves two purposes: It shows you some useful primitives and
// it suppresses warnings for items not being imported
func someUsefulThings() {
	// Creates a random UUID
	f := uuid.New()
	userlib.DebugMsg("UUID as string:%v", f.String())

	// Example of writing over a byte of f
	f[0] = 10
	userlib.DebugMsg("UUID as string:%v", f.String())

	// takes a sequence of bytes and renders as hex
	h := hex.EncodeToString([]byte("fubar"))
	userlib.DebugMsg("The hex: %v", h)

	// Marshals data into a JSON representation
	// Will actually work with go structures as well
	d, _ := json.Marshal(f)
	userlib.DebugMsg("The json data: %v", string(d))
	var g uuid.UUID
	json.Unmarshal(d, &g)
	userlib.DebugMsg("Unmashaled data %v", g.String())

	// This creates an error type
	userlib.DebugMsg("Creation of error %v", errors.New(strings.ToTitle("This is an error")))

	// And a random RSA key.  In this case, ignoring the error
	// return value
	var key *userlib.PrivateKey
	key, _ = userlib.GenerateRSAKey()
	userlib.DebugMsg("Key is %v", key)
}

// Helper function: Takes the first 16 bytes and
// converts it into the UUID type
func bytesToUUID(data []byte) (ret uuid.UUID) {
	for x := range ret {
		ret[x] = data[x]
	}
	return
}

// User structure contains relevant information for a user
type User struct {
	Username      string
	Key           []byte
	UUID          uuid.UUID
	RSAPrivateKey userlib.PrivateKey
}

// encryptDatastore encrypts the data with ek, generates hmac
// and fills datastore[ik].
func encryptDatastore(ik, ek, data []byte) {
	// Generate a new IV for encryption.
	iv := userlib.RandomBytes(userlib.BlockSize)
	// Generate the cipher of data using IV and EK.
	ciphertext := make([]byte, len(data))
	cipher := userlib.CFBEncrypter(ek, iv)
	cipher.XORKeyStream(ciphertext, data)
	// Compute HMAC of IK, IV and ecrypted text.
	hmac := userlib.NewHMAC(ek)
	hmac.Write(ik)
	hmac.Write(iv)
	hmac.Write(ciphertext)
	value := append(hmac.Sum(nil), iv...)
	value = append(value, ciphertext...)
	userlib.DatastoreSet(hex.EncodeToString(ik), value)
}

// decryptDatastore decrypts the data with ek at index ik.
// It establishes integrity of the value too.
func decryptDatastore(ik, ek []byte) ([]byte, bool) {
	// Fetch the data at ik
	value, ok := userlib.DatastoreGet(hex.EncodeToString(ik))
	if !ok || len(value) < userlib.HashSize+userlib.BlockSize {
		return nil, false
	}
	iv := value[userlib.HashSize:][:userlib.BlockSize]
	ciphertext := value[userlib.HashSize+userlib.BlockSize:]
	// Check integrity of the data.
	hmac := userlib.NewHMAC(ek)
	hmac.Write(ik)
	hmac.Write(iv)
	hmac.Write(ciphertext)
	if !userlib.Equal(hmac.Sum(nil), value[:userlib.HashSize]) {
		return nil, false
	}
	// The data in intact. Now decrypt it.
	data := make([]byte, len(ciphertext))
	cipher := userlib.CFBDecrypter(ek, iv)
	cipher.XORKeyStream(data, ciphertext)
	return data, true
}

// InitUser creates a user.  It will only be called once for a user
// (unless the keystore and datastore are cleared during testing purposes)
// It should store a copy of the userdata, suitably encrypted, in the
// datastore and should store the user's public key in the keystore.
// The datastore may corrupt or completely erase the stored
// information, but nobody outside should be able to get at the stored
// User data: the name used in the datastore should not be guessable
// without also knowing the password and username.
// You are not allowed to use any global storage other than the
// keystore and the datastore functions in the userlib library.
// You can assume the user has a STRONG password.
func InitUser(username string, password string) (userdataptr *User, err error) {
	var userdata User
	userdata.Username = username
	// Generate a uuid for the user.
	userdata.UUID = uuid.New()
	// Generate the index key.
	hasher := userlib.NewSHA256()
	hasher.Write([]byte(username + password))
	ik := hasher.Sum(nil)
	if _, ok := userlib.DatastoreGet(hex.EncodeToString(ik)); ok {
		return nil, errors.New("datastore is full")
	}
	// Generate RSA key pair for the user.
	privkey, err := userlib.GenerateRSAKey()
	if err != nil {
		return nil, err
	}
	pubkey := privkey.PublicKey
	// Store the public key in Keystore against username.
	// But first check if username is available.
	if _, ok := userlib.KeystoreGet(username); ok {
		return nil, errors.New("username is taken")
	}
	userlib.KeystoreSet(username, pubkey)
	userdata.RSAPrivateKey = *privkey
	// Generate encryption key for first level file blocks.
	salt := userlib.RandomBytes(userlib.HashSize)
	userdata.Key = userlib.Argon2Key([]byte(password), salt, uint32(userlib.AESKeySize))
	// Generate IV and encryption key for user structure.
	ek := userlib.Argon2Key([]byte(password), []byte("badwolf"), uint32(userlib.AESKeySize))
	// Marshal the userdata and fill datastore
	data, err := json.Marshal(userdata)
	if err != nil {
		return nil, err
	}
	encryptDatastore(ik, ek, data)
	return &userdata, err
}

// GetUser fetches the user information from the Datastore.  It should
// fail with an error if the user/password is invalid, or if the user
// data was corrupted, or if the user can't be found.
func GetUser(username string, password string) (userdataptr *User, err error) {
	// Generate the index key.
	hasher := userlib.NewSHA256()
	hasher.Write([]byte(username + password))
	ik := hasher.Sum(nil)
	ek := userlib.Argon2Key([]byte(password), []byte("badwolf"), uint32(userlib.AESKeySize))
	value, ok := decryptDatastore(ik, ek)
	if !ok {
		return nil, errors.New("data was either modified or never stored")
	}
	// Unmarshal the returned data
	var userdata User
	if err := json.Unmarshal(value, &userdata); err != nil {
		return nil, err
	}
	return &userdata, nil
}

// Files are organized using three levels of block.

// FileLevel1 structure is the 1st level of files.
type FileLevel1 struct {
	IK []byte // Index Key
	EK []byte // Encryption Key
}

// FileLevel2 structure is the 2nd level of files.
type FileLevel2 struct {
	UUID    uuid.UUID
	Length  int
	Appends int
	IK      []byte
	EK      []byte
}

// FileLevel3 structure is the 3rd level of files.
type FileLevel3 struct {
	IK       []byte
	EK       []byte
	FileData []byte
}

func getRandomDatastoreID() (id []byte) {
	id = userlib.RandomBytes(userlib.HashSize)
	for {
		if _, ok := userlib.DatastoreGet(hex.EncodeToString(id)); !ok {
			return
		}
		id = userlib.RandomBytes(userlib.HashSize)
	}
}

// StoreFile stores a file in the datastore.
// The name of the file should NOT be revealed to the datastore!
func (userdata *User) StoreFile(filename string, data []byte) {
	// Generate the 1st level block.
	hasher := userlib.NewSHA256()
	hasher.Write(append(userdata.Key, filename...))
	ik := hasher.Sum(nil)
	var fileL1 FileLevel1
	fileL1.IK = getRandomDatastoreID()
	fileL1.EK = userlib.Argon2Key(userdata.Key, fileL1.IK, uint32(userlib.AESKeySize))
	// Marshal and put the fileL1 in datastore.
	marshalled, err := json.Marshal(fileL1)
	if err != nil {
		return
	}
	encryptDatastore(ik, userdata.Key, marshalled)

	// Generate the 2nd level block.
	var fileL2 FileLevel2
	fileL2.UUID = userdata.UUID
	fileL2.Length = len(data)
	fileL2.Appends = 1
	fileL2.IK = getRandomDatastoreID()
	fileL2.EK = userlib.Argon2Key(fileL1.EK, fileL2.IK, uint32(userlib.AESKeySize))
	// Marshal and put the fileL2 in datastore.
	marshalled, err = json.Marshal(fileL2)
	if err != nil {
		return
	}
	encryptDatastore(fileL1.IK, fileL1.EK, marshalled)

	// Generate the 2nd level block.
	var fileL3 FileLevel3
	// Keep the zeros in IK and EK here.
	fileL3.IK = make([]byte, userlib.HashSize)
	fileL3.EK = make([]byte, userlib.AESKeySize)
	fileL3.FileData = data
	// Marshal and put the fileL3 in datastore.
	marshalled, err = json.Marshal(fileL3)
	if err != nil {
		return
	}
	encryptDatastore(fileL2.IK, fileL2.EK, marshalled)
}

// AppendFile adds on to an existing file.
//
// Append should be efficient, you shouldn't rewrite or reencrypt the
// existing file, but only whatever additional information and
// metadata you need.
func (userdata *User) AppendFile(filename string, data []byte) (err error) {
	// Fetch the 1st level block.
	hasher := userlib.NewSHA256()
	hasher.Write(append(userdata.Key, filename...))
	ik := hasher.Sum(nil)
	marshalled, ok := decryptDatastore(ik, userdata.Key)
	if !ok {
		return errors.New("data was either modified or never stored")
	}
	var fileL1 FileLevel1
	if err := json.Unmarshal(marshalled, &fileL1); err != nil {
		return err
	}

	// Fetch the 2nd level block.
	marshalled, ok = decryptDatastore(fileL1.IK, fileL1.EK)
	if !ok {
		return errors.New("data was either modified or never stored")
	}
	var fileL2 FileLevel2
	if err := json.Unmarshal(marshalled, &fileL2); err != nil {
		return err
	}

	// Create a new append block.
	var fileL3 FileLevel3
	fileL3.IK, fileL3.EK = fileL2.IK, fileL2.EK
	fileL3.FileData = data
	// Change the 2nd level block.
	fileL2.Length += len(data)
	fileL2.Appends++
	fileL2.IK = getRandomDatastoreID()
	fileL2.EK = userlib.Argon2Key(fileL1.EK, fileL2.IK, uint32(userlib.AESKeySize))
	// Marshal and put the fileL3 in datastore.
	marshalled, err = json.Marshal(fileL3)
	if err != nil {
		return err
	}
	encryptDatastore(fileL2.IK, fileL2.EK, marshalled)
	// Marshal and put the fileL2 in datastore.
	marshalled, err = json.Marshal(fileL2)
	if err != nil {
		return err
	}
	encryptDatastore(fileL1.IK, fileL1.EK, marshalled)

	return
}

// LoadFile loads a file from the Datastore.
//
// It should give an error if the file is corrupted in any way.
func (userdata *User) LoadFile(filename string) (data []byte, err error) {
	// Fetch the 1st level block.
	hasher := userlib.NewSHA256()
	hasher.Write(append(userdata.Key, filename...))
	ik := hasher.Sum(nil)
	marshalled, ok := decryptDatastore(ik, userdata.Key)
	if !ok {
		return nil, errors.New("data was either modified or never stored")
	}
	var fileL1 FileLevel1
	if err := json.Unmarshal(marshalled, &fileL1); err != nil {
		return nil, err
	}

	// Fetch the 2nd level block.
	marshalled, ok = decryptDatastore(fileL1.IK, fileL1.EK)
	if !ok {
		return nil, errors.New("data was either modified or never stored")
	}
	var fileL2 FileLevel2
	if err = json.Unmarshal(marshalled, &fileL2); err != nil {
		return nil, err
	}

	data = make([]byte, fileL2.Length)
	var fileL3 FileLevel3
	ik, ek, offset := fileL2.IK, fileL2.EK, fileL2.Length
	for i := 0; i < fileL2.Appends; i++ {
		marshalled, ok = decryptDatastore(ik, ek)
		if !ok {
			return nil, errors.New("data was either modified or never stored")
		}
		if err = json.Unmarshal(marshalled, &fileL3); err != nil {
			return nil, err
		}
		// No need to check bounds since, integrity was intact.
		copy(data[offset-len(fileL3.FileData):offset], fileL3.FileData)

		// Go forward with another IK and EK
		ik, ek = fileL3.IK, fileL3.EK
		offset -= len(fileL3.FileData)
	}
	return
}

// sharingRecord to serialized/deserialize in the data store.
type sharingRecord struct {
	Msg []byte
	Sig []byte
}

// ShareFile creates a sharing record, which is a key pointing to something
// in the datastore to share with the recipient.
// This enables the recipient to access the encrypted file as well
// for reading/appending.
// Note that neither the recipient NOR the datastore should gain any
// information about what the sender calls the file.  Only the
// recipient can access the sharing record, and only the recipient
// should be able to know the sender.
func (userdata *User) ShareFile(filename string, recipient string) (
	msgid string, err error) {
	// Fetch the 1st level block.
	hasher := userlib.NewSHA256()
	hasher.Write(append(userdata.Key, filename...))
	ik := hasher.Sum(nil)
	marshalled, ok := decryptDatastore(ik, userdata.Key)
	if !ok {
		return "", errors.New("data was either modified or never stored")
	}

	// Fetch the public key of recipient.
	pubkey, ok := userlib.KeystoreGet(recipient)
	if !ok {
		return "", errors.New("recipient is not registered in keystore")
	}

	// marshalled is exactly the FileLevel1, so send this.
	// Encrypt marshalled using public key of recipient.
	var sharingData sharingRecord
	sharingData.Msg, err = userlib.RSAEncrypt(&pubkey, marshalled, []byte("sharing"))
	if err != nil {
		return "", err
	}

	// Sign the cipher using private key of sender.
	sharingData.Sig, err = userlib.RSASign(&userdata.RSAPrivateKey, marshalled)
	if err != nil {
		return "", err
	}

	// Generate a random location on the DataStore
	msgid = hex.EncodeToString(getRandomDatastoreID())

	// Marhsal the sharingData and put in datastore.
	marshalled, err = json.Marshal(sharingData)
	if err != nil {
		return "", err
	}
	userlib.DatastoreSet(msgid, marshalled)

	return
}

// ReceiveFile receives the file from the sharer.
// Note recipient's filename can be different from the sender's filename.
// The recipient should not be able to discover the sender's view on
// what the filename even is!  However, the recipient must ensure that
// it is authentically from the sender.
func (userdata *User) ReceiveFile(filename string, sender string,
	msgid string) error {
	// Fetch the sharingData from datastore.
	marshalled, ok := userlib.DatastoreGet(msgid)
	if !ok {
		return errors.New("msgid points to empty location in datastore")
	}
	var sharingData sharingRecord
	if err := json.Unmarshal(marshalled, &sharingData); err != nil {
		return err
	}
	// Decrpyt the data
	marshalled, err := userlib.RSADecrypt(&userdata.RSAPrivateKey, sharingData.Msg, []byte("sharing"))
	if err != nil {
		return err
	}
	// Verify the signature of sender.
	pubkey, ok := userlib.KeystoreGet(sender)
	if !ok {
		return errors.New("sender is not registered in keystore")
	}
	err = userlib.RSAVerify(&pubkey, marshalled, sharingData.Sig)
	if err != nil {
		return err
	}
	// Generate 1st level block index key and store marshalled.
	hasher := userlib.NewSHA256()
	hasher.Write(append(userdata.Key, filename...))
	ik := hasher.Sum(nil)
	encryptDatastore(ik, userdata.Key, marshalled)

	return nil
}

// RevokeFile removes access for all others.
func (userdata *User) RevokeFile(filename string) (err error) {
	// Get the data stored at the file.
	data, err := userdata.LoadFile(filename)
	if err != nil {
		return err
	}

	// Now delete all locations of file and its metadata.
	// Fetch the 1st level block.
	hasher := userlib.NewSHA256()
	hasher.Write(append(userdata.Key, filename...))
	ik := hasher.Sum(nil)
	marshalled, ok := decryptDatastore(ik, userdata.Key)
	if !ok {
		return errors.New("data was either modified or never stored")
	}
	var fileL1 FileLevel1
	if err := json.Unmarshal(marshalled, &fileL1); err != nil {
		return err
	}
	// Clear 1st level block's location.
	userlib.DatastoreDelete(hex.EncodeToString(ik))

	// Fetch the 2nd level block.
	marshalled, ok = decryptDatastore(fileL1.IK, fileL1.EK)
	if !ok {
		return errors.New("data was either modified or never stored")
	}
	var fileL2 FileLevel2
	if err = json.Unmarshal(marshalled, &fileL2); err != nil {
		return err
	}
	// Clear 2nd level block's location.
	userlib.DatastoreDelete(hex.EncodeToString(fileL1.IK))

	// Clear 3rd level blocks.
	ik, ek := fileL2.IK, fileL2.EK
	var fileL3 FileLevel3
	for i := 0; i < fileL2.Appends; i++ {
		marshalled, ok = decryptDatastore(ik, ek)
		if !ok {
			return errors.New("data was either modified or never stored")
		}
		if err = json.Unmarshal(marshalled, &fileL3); err != nil {
			return err
		}
		// Clear the location
		userlib.DatastoreDelete(hex.EncodeToString(ik))

		// Go forward with another IK and EK
		ik, ek = fileL3.IK, fileL3.EK
	}

	// Now restore the file.
	userdata.StoreFile(filename, data)

	return
}
